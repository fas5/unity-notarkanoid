﻿using UnityEngine;
using System.Collections;

public class TitleScreen : MonoBehaviour {

    private bool displayLabel = false;
    public GUISkin layout;
    public Font myFont;
    

    void Start()
    {
        StartCoroutine(FlashLabel());
    }

	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            Application.LoadLevel("main_scene");
        } 
        else if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
	}

    void OnGUI()
    {
        GUI.skin = layout;
        GUI.skin.font = myFont;
        var centeredStyle = GUI.skin.GetStyle("Label");
        centeredStyle.alignment = TextAnchor.UpperCenter;
        if (displayLabel)
        {
            GUI.Label(new Rect(Screen.width / 2 - 140, Screen.height/2, 300, 100), "<size=15>Press RETURN to PLAY!</size>", centeredStyle);
        }
        GUI.Label(new Rect(Screen.width / 2 - 215, Screen.height - 20, 450, 100), "<size=8>Music: Arcade Music Loop (by joshuaempyre)</size>", centeredStyle);
    }

    // util
    public IEnumerator FlashLabel()
    {
        while (true)
        {
            displayLabel = true;
            yield return new WaitForSeconds(0.5f);
            displayLabel = false;
            yield return new WaitForSeconds(0.5f);
        }
    }
}
