﻿using UnityEngine;
using System.Collections;

public class Racket : MonoBehaviour {

    private float h;
    
    public float speed = 150;

    public AudioClip hitSound;
    private AudioSource audioSource;

    void FixedUpdate()
    {
        h = Input.GetAxisRaw("Horizontal");
        GetComponent<Rigidbody2D>().velocity = Vector2.right * h * speed;
    }

    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.name == "ball")
        {
            audioSource.PlayOneShot(hitSound);
        }        
    }
}
