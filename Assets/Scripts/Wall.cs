﻿using UnityEngine;
using System.Collections;

public class Wall : MonoBehaviour {

    public AudioClip hitSound;
    private AudioSource audioSource;

    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.name == "ball")
        {
            audioSource.PlayOneShot(hitSound);
        }
    }

}
