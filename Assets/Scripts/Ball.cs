﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {

    private float x;
    private Vector2 direction;

    public float speed = 100.0f;

    public AudioClip ballDestroyedSound;
    private AudioSource audioSource;

	// Use this for initialization
	void Start () {
        GetComponent<Rigidbody2D>().velocity = Vector2.up * speed;
	}

    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    void FixedUpdate()
    {
        if (transform.position.y < -130)
        {
            //StartCoroutine("PlaySoundAndResetBall");
            audioSource.PlayOneShot(ballDestroyedSound);
            //yield return new WaitForSeconds(ballDestroyedSound.length);
            GetComponent<Rigidbody2D>().velocity = Vector2.up * speed;
            GetComponent<Rigidbody2D>().position = new Vector2(0.1f, -90f);
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        // Hit the Racket?
        if (collision.gameObject.name.Equals("racket"))
        {
            // Calculate hit Factor
            x = hitFactor(transform.position,
                          collision.transform.position,
                          ((BoxCollider2D)collision.collider).size.x);

            // Calculate direction, set length to 1
            direction = new Vector2(x, 1).normalized;

            // Set Velocity with dir * speed
            GetComponent<Rigidbody2D>().velocity = direction * speed;
        }
    }

    // util
    float hitFactor(Vector2 ballPos, Vector2 racketPos, float racketWidth)
    {
        // 1  -0.5  0  0.5   1  <- x value
        // ===================  <- racket
        return (ballPos.x - racketPos.x) / racketWidth;
    }

    public IEnumerator PlaySoundAndResetBall()
    {
        audioSource.PlayOneShot(ballDestroyedSound);
        yield return new WaitForSeconds(ballDestroyedSound.length);
        GetComponent<Rigidbody2D>().velocity = Vector2.up * speed;
        GetComponent<Rigidbody2D>().position = new Vector2(0.1f, -90f);
    }
}
