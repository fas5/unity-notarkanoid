﻿using UnityEngine;
using System.Collections;

public class Block : MonoBehaviour {

    public AudioClip destroySound;

    void OnCollisionEnter2D(Collision2D collision)
    {
        AudioSource.PlayClipAtPoint(destroySound, transform.position);
        // Destroy the whole Block
        Destroy(gameObject, destroySound.length);
    }

}
